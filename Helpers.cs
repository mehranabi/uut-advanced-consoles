﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleQs
{
    class Helpers
    {
        public static double ToRadians(double angle)
        {
            return angle * Math.PI / 180;
        }

        public static double ToDegrees(double angle)
        {
            return angle * 180 / Math.PI;
        }

        public static double Motamam(double angle)
        {
            return 90 - angle;
        }
    }
}
