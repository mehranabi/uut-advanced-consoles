﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleQs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome :)");
            Run();
        }

        private static void Run()
        {
            Console.WriteLine("----------------------------------------------");
            Console.WriteLine("Select your option:");
            Console.WriteLine("1) Hanoi Tower");
            Console.WriteLine("2) Sum 1...N");
            Console.WriteLine("3) C(N, M)");
            Console.WriteLine("4) GCD");
            Console.WriteLine("5) Inverse of Number");
            Console.WriteLine("6) Decimal to Binary");
            Console.WriteLine("7) Palindrom Primes");
            Console.WriteLine("8) Magic Sqaure");
            Console.WriteLine("9) STACK!");
            Console.WriteLine("10) MATRIX!");
            Console.WriteLine("11) VECTOR!");
            Console.WriteLine("0) EXIT");
            int input = Int32.Parse(Console.ReadLine());
            switch (input)
            {
                case 0:
                    System.Environment.Exit(1);
                    break;
                case 1:
                    Console.WriteLine("Please enter initial bar name:");
                    string from = Console.ReadLine();
                    Console.WriteLine("Please enter helper bar name:");
                    string helper = Console.ReadLine();
                    Console.WriteLine("Please enter final bar name:");
                    string to = Console.ReadLine();
                    Console.WriteLine("Please enter count of rings:");
                    int count = Int32.Parse(Console.ReadLine());
                    SolveHanoi(from, to, helper, count);
                    Run();
                    break;
                case 2:
                    Console.WriteLine("Please enter number N:");
                    int n = Int32.Parse(Console.ReadLine());
                    Console.WriteLine(Sum2N(n));
                    Run();
                    break;
                case 3:
                    Console.WriteLine("Please enter number N:");
                    n = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter number M:");
                    int m = Int32.Parse(Console.ReadLine());
                    Console.WriteLine(Comb(n, m));
                    Run();
                    break;
                case 4:
                    Console.WriteLine("Please enter number A:");
                    int a = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter number B: ");
                    int b = Int32.Parse(Console.ReadLine());
                    Console.WriteLine(GDC(a, b));
                    Run();
                    break;
                case 5:
                    Console.WriteLine("Please enter number N:");
                    n = Int32.Parse(Console.ReadLine());
                    Console.WriteLine(MInverse(n, 0));
                    Run();
                    break;
                case 6:
                    Console.WriteLine("Please enter number N:");
                    n = Int32.Parse(Console.ReadLine());
                    Console.WriteLine(ToBinary(n));
                    Run();
                    break;
                case 7:
                    Console.WriteLine("Please enter start number:");
                    int start = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter end number: ");
                    int end = Int32.Parse(Console.ReadLine());
                    FindPPs(start, end);
                    Run();
                    break;
                case 8:
                    Console.WriteLine("Please enter sqaure size:");
                    n = Int32.Parse(Console.ReadLine());
                    MakeMagicSquare(n);
                    Run();
                    break;
                case 9:
                    Console.WriteLine("Enter Stack size:");
                    int size = Int32.Parse(Console.ReadLine());
                    Stack stack = new Stack(size);
                    RunStack(stack);
                    Run();
                    break;
                case 10:
                    TestMatrix();
                    Run();
                    break;
                case 11:
                    TestVector();
                    Run();
                    break;
                default:
                    Console.WriteLine("Invalid OPTION!");
                    Run();
                    break;
            }
        }
        
        private static void TestVector()
        {
            Vector v1 = new Vector(20, 45);
            Console.WriteLine("First Vector:");
            v1.print();

            Vector v2 = new Vector(25, 300);
            Console.WriteLine("Second Vector:");
            v2.print();

            Vector result;

            Console.WriteLine("Sum:");
            result = v1 + v2;
            result.print();
        }

        private static void TestMatrix()
        {
            Random r = new Random();

            int[,] values1 = new int[2, 3] {
                { r.Next(1, 50), r.Next(1, 50), r.Next(1, 50) },
                { r.Next(1, 50), r.Next(1, 50), r.Next(1, 50) }
            };

            int[,] values2 = new int[2, 3] {
                { r.Next(1, 50), r.Next(1, 50), r.Next(1, 50) },
                { r.Next(1, 50), r.Next(1, 50), r.Next(1, 50) }
            };

            Matrix m1 = new Matrix(2, 3, values1);
            Console.WriteLine("First Matrix:");
            m1.print();

            Matrix m2 = new Matrix(2, 3, values2);
            Console.WriteLine("Second Matrix:");
            m2.print();

            Matrix result;

            Console.WriteLine("Sum:");
            result = m1 + m2;
            result.print();

            Console.WriteLine("Substract:");
            result = m1 - m2;
            result.print();

            Console.WriteLine("First Matrix Transpose:");
            result = !m1;
            result.print();
        }

        private static void RunStack(Stack stack)
        {
            Console.WriteLine("1) PUSH");
            Console.WriteLine("2) POP");
            Console.WriteLine("0) Exit");
            int input = Int32.Parse(Console.ReadLine());
            switch (input)
            {
                case 0:
                    return;
                case 1:
                    Console.WriteLine("Enter a number to push it to stack:");
                    int n = Int32.Parse(Console.ReadLine());
                    stack.push(n);
                    RunStack(stack);
                    break;
                case 2:
                    Console.WriteLine(stack.pop());
                    RunStack(stack);
                    break;
                default:
                    Console.WriteLine("Invalid Option!!");
                    RunStack(stack);
                    break;
            }
        }

        private static void SolveHanoi(string from, string to, string helper, int count)
        {
            if (count <= 1)
            {
                Console.WriteLine("{0} == {1} ==> {2}", from, count, to);
            }
            else
            {
                SolveHanoi(from, to, helper, count - 1);
                Console.WriteLine("{0} == {1} ==> {1}", from, count, to);
                SolveHanoi(helper, from, to, count - 1);
            }
        }

        private static long Sum2N(int n)
        {
            if (n == 1)
            {
                return 1;
            }

            return n + Sum2N(n - 1);
        }

        private static int Comb(int n, int m)
        {
            if (m == n || m == 1)
            {
                return 1;
            }

            return Comb(n - 1, m) + Comb(n - 1, m - 1);
        }

        private static int GDC(int a, int b)
        {
            if (b == 0)
            {
                return a;
            }

            int r = a % b;
            return GDC(b, r);
        }

        private static int MInverse(int n, int last)
        {
            if (n % 10 == n)
            {
                return last * 10 + n;
            }

            return MInverse(n / 10, last * 10 + (n % 10));
        }

        private static int ToBinary(int n)
        {
            if (n == 0)
            {
                return 0;
            }

            return (n % 2) + 10 * ToBinary(n / 2);
        }

        private static bool IsPrime(int n)
        {
            if (n <= 1)
            {
                return false;
            }

            if (n == 2)
            {
                return true;
            }

            if (n % 2 == 0)
            {
                return false;
            }

            int sqrt = (int)Math.Floor(Math.Sqrt(n));

            for (int i = 3; i <= sqrt; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }

            return true;
        }

        private static bool IsPalindrom(int n)
        {
            return MInverse(n, 0) == n;
        }

        private static void FindPPs(int start, int end)
        {
            for (int i = start; i <= end; i++)
            {
                if (IsPrime(i) && IsPalindrom(i))
                {
                    Console.WriteLine(i);
                }
            }
        }

        private static void MakeMagicSquare(int n)
        {
            if (n % 2 == 0)
            {
                Console.WriteLine("Input number must be odd!");
                return;
            }

            int[,] square = new int[n, n];
            for (int a = 0; a < n; a++)
            {
                for (int b = 0; b < n; b++)
                {
                    square[a, b] = 0;
                }
            }

            int k = 1;

            int i = 0;
            int j = (n - 1) / 2;

            while (k <= n * n)
            {
                square[i, j] = k++;

                int newI = i - 1;
                int newJ = j - 1;

                if (newI < 0)
                {
                    newI = n - 1;
                }

                if (newJ < 0)
                {
                    newJ = n - 1;
                }

                if (newI >= n)
                {
                    newI = 0;
                }

                if (newJ >= n)
                {
                    newJ = 0;
                }

                if (square[newI, newJ] != 0)
                {
                    newI = i + 1;
                    newJ = j;
                }

                i = newI;
                j = newJ;
            }

            for (int a = 0; a < n; a++)
            {
                for (int b = 0; b < n; b++)
                {
                    Console.Write("{0}\t", square[a, b]);
                }
                Console.WriteLine();
            }
        }
    }
}
