﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleQs
{
    class Stack
    {
        private int[] stack;
        private int pointer = -1;
        private int size;

        public Stack(int size)
        {
            if (size == 0)
            {
                Console.WriteLine("Zero sized stack?! OK :|");
            }

            this.size = size;
            stack = new int[size];
        }

        public void push(int x)
        {
            if (++pointer == size)
            {
                Console.WriteLine("Stack is full!");
                pointer--;
                return;
            }

            stack[pointer] = x;
        }

        public int pop()
        {
            if (pointer < 0)
            {
                Console.WriteLine("Stack is empty!");
                return -1;
            }

            return stack[pointer--];
        }
    }
}
