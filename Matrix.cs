﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleQs
{
    class Matrix
    {
        private int rows;
        private int columns;

        private int[,] matrix;

        public Matrix(int rows, int columns, int[,] values)
        {
            this.rows = rows;
            this.columns = columns;
            this.matrix = values;
        }

        public void setValues(int[,] values)
        {
            this.matrix = values;
        }

        public static Matrix operator +(Matrix a, Matrix b)
        {
            if (a.getRowsCount() != a.getRowsCount())
            {
                Console.WriteLine("Matrixes must be same in the matter of size!");
                return null;
            }

            if (a.getColumnsCount() != a.getColumnsCount())
            {
                Console.WriteLine("Matrixes must be same in the matter of size!");
                return null;
            }

            int[,] aMatrix = a.getMatrix();
            int[,] bMatrix = b.getMatrix();
            int[,] newMatrix = new int[a.getRowsCount(), a.getColumnsCount()];

            for (int i = 0; i < a.getRowsCount(); i++)
            {
                for (int j = 0; j < a.getColumnsCount(); j++)
                {
                    newMatrix[i, j] = aMatrix[i, j] + bMatrix[i, j];
                }
            }

            return new Matrix(a.getRowsCount(), a.getColumnsCount(), newMatrix);
        }

        public static Matrix operator -(Matrix a, Matrix b)
        {
            if (a.getRowsCount() != a.getRowsCount())
            {
                Console.WriteLine("Matrixes must be same in the matter of size!");
                return null;
            }

            if (a.getColumnsCount() != a.getColumnsCount())
            {
                Console.WriteLine("Matrixes must be same in the matter of size!");
                return null;
            }

            int[,] aMatrix = a.getMatrix();
            int[,] bMatrix = b.getMatrix();
            int[,] newMatrix = new int[a.getRowsCount(), a.getColumnsCount()];

            for (int i = 0; i < a.getRowsCount(); i++)
            {
                for (int j = 0; j < a.getColumnsCount(); j++)
                {
                    newMatrix[i, j] = aMatrix[i, j] - bMatrix[i, j];
                }
            }

            return new Matrix(a.getRowsCount(), a.getColumnsCount(), newMatrix);
        }

        public static Matrix operator !(Matrix m)
        {
            int rows = m.getRowsCount();
            int columns = m.getColumnsCount();

            int[,] values = m.getMatrix();
            int[,] newValues = new int[columns, rows];

            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    newValues[i, j] = values[j, i];
                }
            }

            return new Matrix(columns, rows, newValues);
        }

        public int getRowsCount()
        {
            return rows;
        }

        public int getColumnsCount()
        {
            return columns;
        }

        public int[,] getMatrix()
        {
            return matrix;
        }

        public void print()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Console.Write("{0}\t", matrix[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}
