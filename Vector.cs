﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleQs
{
    class Vector
    {
        private double length;
        private double angle;

        public Vector(double length, double angle)
        {
            if (angle < 0)
            {
                Console.WriteLine("Angle must be positive!");
                return;
            }

            if (angle >= 360)
            {
                Console.WriteLine("Angle cannot be greater than 359 degrees!");
                return;
            }

            if (length == 0)
            {
                Console.WriteLine("Length cannot be 0!");
                return;
            }

            this.length = length;
            this.angle = angle;
        }

        public double getLength()
        {
            return length;
        }

        public double getAngle()
        {
            return angle;
        }

        public static double makeAngle(double x, double y)
        {
            double angle = 0;

            if (x == 0)
            {
                if (y > 0)
                {
                    angle = 90;
                }
                else if (y < 0)
                {
                    angle = 270;
                }
                return angle;
            }

            if (y == 0)
            {
                if (x > 0)
                {
                    angle = 0;
                }
                else if (x < 0)
                {
                    angle = 180;
                }
                return angle;
            }

            angle = Math.Atan(y / x);

            if (x > 0 && y > 0)
            {
                angle += 0;
            }
            else if (x > 0 && y < 0)
            {
                angle += 270 + Helpers.Motamam(angle);
            }
            else if (x < 0 && y > 0)
            {
                angle += 90 + Helpers.Motamam(angle);
            }
            else if (x < 0 && y < 0)
            {
                angle += 180;
            }

            return angle;
        }

        public static double makeLength(double x, double y)
        {
            double length;

            if (y == 0)
            {
                return x >= 0 ? x : -x;
            }

            if (x == 0)
            {
                return y >= 0 ? y : -y;
            }

            length = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));

            return length;
        }

        public static Vector operator +(Vector a, Vector b)
        {
            double x = a.getX() + b.getX();
            double y = a.getY() + b.getY();

            double length = makeLength(x, y);
            double angle = makeAngle(x, y);

            return new Vector(length, angle);
        }

        public double getX()
        {
            if (angle == 0)
            {
                return length;
            }

            if (angle < 90)
            {
                return length * Math.Cos(Helpers.ToRadians(angle));
            }

            if (angle == 90)
            {
                return 0;
            }

            if (angle < 180)
            {
                return -(length * Math.Cos(Helpers.ToRadians(180 - angle)));
            }

            if (angle == 180)
            {
                return -length;
            }

            if (angle < 270)
            {
                return -(length * Math.Cos(Helpers.ToRadians(angle - 180)));
            }

            if (angle == 270)
            {
                return 0;
            }

            if (angle < 360)
            {
                return length * Math.Cos(Helpers.ToRadians(360 - angle));
            }

            return 0;
        }

        public double getY()
        {
            if (angle == 0)
            {
                return 0;
            }

            if (angle < 90)
            {
                return length * Math.Sin(Helpers.ToRadians(angle));
            }

            if (angle == 90)
            {
                return length;
            }

            if (angle < 180)
            {
                return -(length * Math.Sin(Helpers.ToRadians(180 - angle)));
            }

            if (angle == 180)
            {
                return 0;
            }

            if (angle < 270)
            {
                return -(length * Math.Sin(Helpers.ToRadians(angle - 180)));
            }

            if (angle == 270)
            {
                return -length;
            }

            if (angle < 360)
            {
                return length * Math.Sin(Helpers.ToRadians(360 - angle));
            }

            return 0;
        }

        public void print()
        {
            Console.WriteLine("Length: {0}", length);
            Console.WriteLine("Angle: {0}", angle);
            Console.WriteLine("X: {0}", getX());
            Console.WriteLine("Y: {0}", getY());
        }
    }
}
